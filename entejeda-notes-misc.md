# logmon # monitoring # 4 # golden # signals

  The 4 Golden Signals, and how to put them into practice | TechTarget
  https://www.techtarget.com/searchapparchitecture/tip/The-4-Golden-Signals-and-how-to-put-them-into-practice
  "frameworks" "Utilization, Saturation, and Errors" - Google Search
  "methodologies" "Utilization, Saturation, and Errors" - Google Search
  https://www.google.com/search?q=%22methodologies%22+%22Utilization%2C+Saturation%2C+and+Errors%22&sca_esv=23c20ded85caa685&sca_upv=1&rlz=1C1GCCA_en___US1110&ei=sKhgZvmZKJ2q5NoPqOulkAE&ved=0ahUKEwj5sYawhsWGAxUdFVkFHah1CRIQ4dUDCBA&uact=5&oq=%22methodologies%22+%22Utilization%2C+Saturation%2C+and+Errors%22&gs_lp=Egxnd3Mtd2l6LXNlcnAiNSJtZXRob2RvbG9naWVzIiAiVXRpbGl6YXRpb24sIFNhdHVyYXRpb24sIGFuZCBFcnJvcnMiMggQIRigARjDBEiNCFCtAlitAnABeACQAQCYAbsBoAG7AaoBAzAuMbgBA8gBAPgBAvgBAZgCAqACwQHCAg4QABiABBiwAxiGAxiKBcICCxAAGIAEGLADGKIEmAMAiAYBkAYEkgcDMS4xoAeOAg&sclient=gws-wiz-serp
  yamltable · PyPI
  https://pypi.org/project/yamltable/

# rust # graphql # router # api

  The Apollo Router - Apollo GraphQL Docs
  https://www.apollographql.com/docs/router/

  GitHub - apollographql/router: A configurable, high-performance routing runtime for Apollo Federation 🚀
  https://github.com/apollographql/router

# perfmon # apm # application # performance # monitoring # vendors # solutions # saas # products # retail # services # appdynamics # kubernetes # k8s

  Kubernetes
  https://docs.appdynamics.com/observability/cisco-cloud-observability/en/cisco-appdynamics-support-for-opentelemetry/opentelemetry-collector/cisco-appdynamics-distribution-of-opentelemetry-collector/deploy-the-cisco-appdynamics-distribution-of-opentelemetry-collector/kubernetes

  Install Kubernetes and App Service Monitoring
  https://docs.appdynamics.com/observability/cisco-cloud-observability/en/kubernetes-and-app-service-monitoring/install-kubernetes-and-app-service-monitoring

# web # terminology # what # is # define # definition # webhook

  What is webhook?

  A WebHook is an HTTP callback: an HTTP POST that occurs when something happens; a simple event-notification via HTTP POST. A web application implementing WebHooks will POST a message to a URL when certain things happen.

# powershell # query # gitlab # pat # personal # access # token

  ```shell
  $pat="<your PAT here>"

  $headers = @{
      'PRIVATE-TOKEN' = $pat
  }

  # Getting a list of projects
  $rsp=$(Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects?per_page=1000" -Headers $headers)

  # list the basic project properties
  $rsp | ConvertFrom-Json | Select name, id
  ```
  
# git # gitlab # errors # breakfix # troubleshooting # username # inconsistent # with # account # name

  gitlab - "Make sure you configure your 'user.email' and 'user.name' in git" when trying to push to git lab - Stack Overflow
  https://stackoverflow.com/questions/54876421/make-sure-you-configure-your-user-email-and-user-name-in-git-when-trying-t

  see:
    windows your gitlab username is inconsistent with gitlab account name - Google Search

# chrome # bookmarklet # javascript # copy # tab # title # and # url

  From: Bookmarklet to copy current page title and url in Markdown format to clipboard, like [title](url) - Usual for posting links to resources in README.md files · GitHub
  https://gist.github.com/bradleybossard/3667ad5259045f839adc

  Slightly modified to not use markdown format

  ```javascript
  javascript:(function() {

  function copyToClipboard(text) {
      if (window.clipboardData && window.clipboardData.setData) {
          /*IE specific code path to prevent textarea being shown while dialog is visible.*/
          return clipboardData.setData("Text", text); 

      } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
          var textarea = document.createElement("textarea");
          textarea.textContent = text;
          textarea.style.position = "fixed";  /* Prevent scrolling to bottom of page in MS Edge.*/
          document.body.appendChild(textarea);
          textarea.select();
          try {
              return document.execCommand("copy");  /* Security exception may be thrown by some browsers.*/
          } catch (ex) {
              console.warn("Copy to clipboard failed.", ex);
              return false;
          } finally {
              document.body.removeChild(textarea);
          }
      }
  }

  var markdown = document.title + '\n' + window.location.href;
  copyToClipboard(markdown);
  })();
  ```

# chrome # bookmarklet # javascript # copy # tab # title # and # url # markdown # format

  [Bookmarklet to copy current page title and url in Markdown format to clipboard, like [title](url) - Usual for posting links to resources in README.md files · GitHub](https://gist.github.com/bradleybossard/3667ad5259045f839adc)

  ```javascript
  javascript:(function() {

  function copyToClipboard(text) {
      if (window.clipboardData && window.clipboardData.setData) {
          /*IE specific code path to prevent textarea being shown while dialog is visible.*/
          return clipboardData.setData("Text", text); 

      } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
          var textarea = document.createElement("textarea");
          textarea.textContent = text;
          textarea.style.position = "fixed";  /* Prevent scrolling to bottom of page in MS Edge.*/
          document.body.appendChild(textarea);
          textarea.select();
          try {
              return document.execCommand("copy");  /* Security exception may be thrown by some browsers.*/
          } catch (ex) {
              console.warn("Copy to clipboard failed.", ex);
              return false;
          } finally {
              document.body.removeChild(textarea);
          }
      }
  }

  var markdown = '[' + document.title + '](' + window.location.href + ')';
  copyToClipboard(markdown);
  })();
  ```