# sre # phonebook # shola # babalola # employees # teammates

  babalolashola.sb@gmail.com
  Phongle21@gmail.com

# sre # onboarding # gitlab # access # requests # cdp # continuous # delivery # platform

  Step-2b Subgroup/Project Info - CDP Community Hub
  https://cdp.docs.t-mobile.com/getting-started/group_info

# employment.t-mobile # splunk # query # todo

  splunk query
  steelthread queries
  how often do these consumers see the timeouts
  what does it mean to appropriately onboard your app into appd
    red metrics are enabled by default
  what constitutes registration in appdynamics
  
  is this an appd specific thing
  or is this from the mast/argos perspective

# employment.t-mobile # team # responsibilities # self # rca # who # poc # point # of # contact # people

  Hi Everyone please note moving forward we are assigning the primary POC for conversations with respective teams. this allocation will be rotated periodically 
   
  CDP/TKE - Le, Phong Iqbal,Iqbal, Gibran
  Pactbot - Babalola, Shola
  Observability - Shamsi, Samir
  RCA - Tejeda, Engelbert
  CC: Verma, Vikas Kolski, Steve Castle, Brian

# employment.t-mobile # jira # worklog # EDSRE-246 # obs.02 # red # metrics # business # transactions # policy # policies # appd # dynamics # api # requests # access
  
  20240612

  AppDynamics API Requests - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/display/ESAKB/AppDynamics+API+Requests

  Here's a Business Application in Appdymanics
  Here are the RED metrics for this BA

  Reach out to Michael Chan

  Template
    7 Health Checks
    Are RED metrics covered here?
      If not
        We can

  Team collaboration:  needed with Observability team.

  Rate, Error and Duration (RED) based metrics for infrastructure and business transactions

  Infrastructure (configuration item, CI) - pods, nodes, deployments etc 
  (new USE metrics based policy)

  Application (APM) - artifacts running on infrastructure, Business transactions, Business services

  Standard Monitor naming convention <-- categorize monitors into RED metrics

  Argos/MAST <-- Splunk, AppDynamics, SiteScope, etc

  Policy Criteria

  Business Criticality and RED metrics based monitors in MAST

  Enforcement

  Onboarding into Argos should require monitors to follow standard RED metrics based monitor naming convention

  Scope - 5KF

  Not in scope - checking if the threshold for those metrics are correct

  To discuss with Observability team:

  Document should define our applications and artifacts.  

  What are our RED metrics
  What are the metrics sources for infrastructure and services
  Do we have a process for evaluating the importance of our metrics?
  Are we sharing nomenclature and naming across teams?  
  If so what terms do we need to adopt?
  How/Are we monitoring metrics across environments?  (prod/non-prod)
  Any details on synthetic metrics?

# sre # observability # design # goals # slo # service # level # objectives # future

  The Enterprise Observability Standard:

  Envisions a T-Mobile future where robust observability capabilities are baked into all systems and processes that provide comprehensive insight to system health, performance and behavior that include:
  
  - Fast, reliable dashboards and alarming from metrics
  - Quick isolation of the source of errors or latency
  - Easy correlation of metrics, logs, and traces to events to correlate issues to changes
  - Easy correlation from tracing to logs to discover root cause of issues

# employment.t-mobile # jira # worklog # EDSRE-173 # operational # readiness # orr # checklist # python # convert # markdown # to # excel # columns # headers # gitlab # pipelines # yml # ci

  ```yaml
  ---
  include:
    - project: "tmobile/templates"
      file:
        ## tmo from cdp
        - "/gitlab-ci/.tmo.global.common.gitlab-ci.yml"

  image: alpine:${CDP_ALPINE_VERSION}

  variables:
    ## refer to https://techportal.t-mobile.com/display/MS/CICD#CICD-CICDVariables for details
    ASSET_TYPE: GENERIC
    GIT_GUARDIAN_FULL_HISTORY: "true" #from CDP

  workflow:
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      - if: $CI_COMMIT_BRANCH
      - if: $CI_COMMIT_TAG

  ## stages
  stages:
    - tmo
    - test
    - publish

  orr-checklist:
    stage: publish
    script:
      - if ! exists "python3" ; then installPackage "python3"; fi;
      - if ! exists "py3-pip" ; then installPackage "py3-pip"; fi;
      - pip3 install mael
      - mkdir -p orr-checklist/config
      - echo "$MAEL_CONFIG" > orr-checklist/config/columns.yml
      - cp content/standards/orr/orr_*.md orr-checklist/
      - mael build orr-checklist
    artifacts:
      paths:
        - orr-checklist/output/orr-checklist.xlsx
    tags:
      - $CDP_K8S_TINY
  ```

# sre # onboarding # git # guardian # accounts # access
  
  From Le, Phon (Tony):

  GitGuardian
      - Join Teams Channel: [AppSec Test Support - CyberSecurity](https://teams.microsoft.com/l/channel/19%3ABsYAblCrcwX_yhk6Tj02IGG0zgHQSmj8eIMnHO4KYdI1%40thread.tacv2/?groupId=003ebe39-8ab2-4dd6-abbe-b66869c3f116&tenantId=be0f980b-dd99-4b19-bd7b-bc71a09b026c)
          - General: Request access to GitGuardian. Provide your NTID & Email
      - https://confluencesw.t-mobile.com/display/AADS/GitGuardian+%28GGShield%29+Setup
      - https://confluencesw.t-mobile.com/display/TWOR/GitGuardian+Access
  You need to authenticate to GitGuardian. 
  Check with that Teams channel if you have an account and if so, if it's active. 
  If not, they will need to grant you access. 

# employment.t-mobile # my # sublime # text # settings

  ```json
  {
      "ignored_packages":
      [
          "Vintage",
      ],
      "default_line_ending": "unix",
      "detect_indendation": false,
      "tab_size": 2,
      "translate_tabs_to_spaces": true,
      "word_wrap": false,
      "theme": "Adaptive.sublime-theme",
      "color_scheme": "Packages/Color-Scheme - Defualt/Monokai.sublime-color-scheme",
      "font_size": 18,
      "index_files": true,
      "sublime_merge_path": "C:\\Users\\ETejeda2\\tools\\SublimeMerge\\sublime_merge.exe",
  }
  ```

# sre # documentation # enterprise # standards

  Enterprise Standards - IT-DDA - T-Mobile
  https://confluencesw.t-mobile.com/display/ITDDA/Enterprise+Standards

  Enterprise Architecture Principles
  https://devcenter.t-mobile.com/documents/60f7143398b00500088587a0?name=Enterprise-Architecture-Principles&filePath=/Principle-Documentations&sectionName=Principle-Documentations

# sre # applications # list # of # appid

  T-Mobile - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/display/ESAKB/T-Mobile

# sre # logmon # standards # logging # fields # common # core # user-defined # defined

  Common core user-defined fields:

  Reserved Names Core Logging Names Description

  event_time event_date Event logged timestamp (sample value
  - 1498741969232)
  host_endpoint proxy_url URL of the API
  event_id event_id Unique id generated in apigee for the event
  service flow_name Api name
  component component Hardcoded to “coreapi”
  event_type event_type Hardcoded to “main” and “payload” (I think this is
  added as per the direction from splunk team for the
  search query)
  attachment_format payload_type Hardcoded to xml or json based on the contentType
  attachment attachment Source/Target request and response payload
  organization Org of the api
  environment Env of the api
  apiproxy Proxy name
  service_transaction_id Unique id generated in apigee for the transaction
  interaction_id Interactionid for the transaction (sent by api
  consumers)
  workflow_id workflowid for the transaction (sent by api consumers)
  activity_id Activityid for the transaction (sent by api consumers)
  result SUCCESS or ERROR
  http_method Http method of the api
  sender_id Senderid of the consumer
  channel_id Channelid of the consumer
  application_id applicationId of the consumer
  application_user_id applicationUserId (sent by consumers)
  session_id Sessionid (sent by consumers)
  inventory_channel_code User details from OES
  sales_channel_code
  sales_subchannel_code
  sub_channel_category
  dealer_code Dealercode (sent by consumers)
  master_dealer_code MasterDealerCode (sent by consumers)
  store_id Storeid (sent by consumers)
  operation_path Concatenation of request uri and method
  proxy_req_start Timestamp when API started receiving the request
  from consumers
  proxy_req_end Timestamp when API finished receiving the request
  from consumers
  proxy_resp_start Timestamp when API started sending the response to consumers
  proxy_resp_end Timestamp when API started sending the response to
  consumers
  caller_ip IP address of the requester
  event_sub_type Hardcoded to “proxyRequestPayload”,
  “proxyResponsePayload”, “ targetRequestPayload”
  and “targetResponsePayload” (I think this is added as
  per the direction from splunk team for the search
  query)
  target_call_type Hardcoded to “ServiceCallout” and “Standard” based
  on the type of call APIGEE is making to backend.
  target_server URL of the target service
  target_req_start Timestamp when API started sending the request to
  backend
  target_req_end Timestamp when API started receiving the request to
  backend
  target_resp_start Timestamp when API started receiving the response
  from backend
  target_resp_end Timestamp when API started receiving the response
  from backend
  target_time_taken Time taken by the backend to process the request
  targets_total_time_taken Total time taken by the backend to process the
  request
  targetMetadata Accumulated information of all the above target
  details.
  proxy_total_time_taken Total time taken by API to process the request
  proxy_time_taken Total time taken by the API – total time taken by the
  backend

  The following guidelines are outlined to guide developers when creating error messages:

  • Error messages need to be aggregated when reported on. Therefore, dynamic data needs to be maintained outside of the context of the error text.
  • Code inspections should flag punctuation and spelling issues.
  • There should never be an instance of "Unknown Error".
    All errors should provide a starting point for an investigation 

  Field Description ERROR Description
  error_source Represents Client/Consumer Endpoint.
  Examples of a consumer endpoint (i.e. SAPCARE)
  error_target Represents Server/Producer Endpoint.
  Examples of a producer endpoint (i.e. Database Application,
  Order Manager, BSCS, etc.)
  error_code Should be structured and consistent (i.e. ON-1XXX, OM-2XXX,
  BSCS-3XXX, etc.)
  Status code should communicate three main conclusions –
  Success, Failure due to Error, and Failure due to Negative
  Response (i.e. wrong cart id)
  substatus_code Should be structured and consistent
  SubStatus code should provide further break down if possible of
  the error. For example, if the error code denotes a type of
  Negative Response, the SubStatus code can be used to explain
  what type of Negative Response.
  error_definition Meaningful description of the error which describes the reason
  for the unexpected change in the thread of execution
  The error definition is a well known string that is tied to a given
  error code. The string should not include any dynamic data and
  should provide concise explanation of the error condition.
  Ex.: Validation error – invalid channel identifier
  In the error explanation field you can provide dynamic
  information that further clarifies the given error
  error_explanation More details describing the error condition when required.
  The format of this field should be in key/value pair format
  separated by semi-colon. Ex.
  key1=val1;key2=val2;…..;keyN=valN
  error_exception Data supporting the detailed trace. This can include tracebacks,
  etc…

  see:
  Logs and Metrics. At last Wednesday’s Go meetup, one of… | by Cindy Sridharan | Medium
  https://copyconstruct.medium.com/logs-and-metrics-6d34d3026e38

# sre # ansible # best # practices
  
  From: Jido Best Practices - Autonomous Platforms - T-Mobile
  https://confluencesw.t-mobile.com/display/AP/Jido+Best+Practices

  Repository File and Directory Structure
  Please review the Ansible best practices from the Ansible documentation at http://docs.ansible.com/playbooks_best_practices.html.

  It is worth noting that if your playbook is does not show-up when creating a job template it is likely due to a code organization issue. Please double check that everything is structured properly.

  Example of Code Organization
  Sample
  group_vars/
    prd_environment.yml       # here we assign variables for particular environments
    qa_environment.yml
    tst_environment.yml
  library/
  module_utils/
  filter_plugins/
  roles/
    install_java/
    install_ibm_websphere/
    restart_service/
    update_application_config/
    
  deploy_my_application.yml            # the playbook. this must be outside of any other ansible directories
  Best Practices
  Consider setting “forks” on a job template to larger values to increase parallelism of execution runs. For more information on tuning Ansible, see the Ansible blog.
  For a Continuous Integration system, such as Jenkins, to spawn an Tower job, it should make a curl request to a job template. The credentials to the job template should not require prompting for any particular passwords. Refer to AWX CLI Ansible Tower documentation for configuration and usage instructions.
  Some Don'ts
  Playbooks should not use the vars_prompt feature, as Tower does not interactively allow for vars_prompt questions. If you must use vars_prompt, refer to and make use of the Surveys functionality of Tower.
  Playbooks should not use the pause feature of Ansible without a timeout, as Tower does not allow for interactively cancelling a pause. If you must use pause, ensure that you set a timeout.

# sre

  APM0100188
  Navigate to SeviceNow
  https://tmus.service-now.com/nav_to.do?uri=%2Fhome_splash.do%3Fsysparm_direct%3Dtrue

# sre # observability # logmon # perfmon # apm # application # performance # monitoring # application # best # practices

  From: Best Practices and Recommendations - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/display/ESAKB/Best+Practices+and+Recommendations

  Best Practices and Recommendations
  Created by Chan, Michael, last modified on Jan 25, 2021
  Below are a collection of recommendations and best practices when instrumenting applications in AppDynamics for core APM monitoring. This list is by no means complete, far from it, but instead, allows for you to fully leverage the capabilities of the tool that enables you to meet your business objectives very quickly with regards to application performance monitoring. 

  Utilize existing APM strategy that drives application performance management success
  Reflect on your APM strategy
  Customize your AppDynamics implementation to meet your unique needs
  Customer applications are grouped correctly within AppDynamics applications
  Business transactions have been limited to the most important and critical functions of the business
  Flow maps have been optimized 
  Define critical issue templates
  Align your health rules with your KPIs and APM strategy
  Identify the criteria for triggering alerts
  Think about critical violations of rules
  Health rules, policies, and actions ultimately help with automation
  Dont overwhelm your team
  Think about what is important and what can wait!
  Continue to drive your APM success


  Thoughts on initial Business Transaction configuration
  KPI Brainstorm

  Consider your KPIs

  Stakeholder input

  Break down silos and gather mission critical application functionality for each group

  Transaction Discovery

  Start with 10-15 most business critical transactions 

  Reduce noise

  Realize that even 10-15 business transaction will record a lot of data

  Consider the financial impact

  Too much data turns into noise and impacts your bottom line

  10 steps in developing Business Transactions
  Identify the business transactions that each team has singled out as important for each tier they need to monitor.

  Validate your business transaction list throughout your organization.

  Configure custom configuration rules for business transactions for each tier.

  Enable Business Transaction (BT) lockdown.

  Collect all other business transactions in a "catch-all-others" or "All Other Traffic" category.

  Promote your top business transactions from the "All-Other-traffic" as first class Business Transactions.

  Leave non-critical application functionality in the "catch-all-others" or "All Other Traffic" category.

  Delete any unnecessary business transactions.

  Explicitly define transactions manually as some transactions may not be automatically discovered.

  Review your business transactions periodically and modify custom configuration rules as needed. 

  With Regards to Health Rules....
  Best practices for defining health rules and alerts:
  Ensure that your health rules are sending alerts to the right people
  Health rules should always alert the team/SME responsible for remediation
  Keep it a practice to consult your stakeholders and application owners to find out what is critical to their success and who is responsible for problem remediation
  How do these help your SOC/NOC, operations, and development team?
  You won't overwhelm your SOC/NOC, operations, and development team with lots of events during the night or during busy work hours
  The right team/SME should be working on the right issues at the right time
  A properly configured health rule will keep your customers happy
  Fine tune your health rules over time....
  Your key performance indicators will help show when random variations are occurring which could indicate that more tuning is needed.
  Turning your health rules is an iterative process and while performing this maintenance, only send actions and alerts to yourself. 
  Once there is an agreement on the new thresholds then notifications should go to the proper owners. 

# sre # observability # logmon # perfmon # apm # application # performance # monitoring # application # maturity # howto # implementation # steps # appdynamics # appd
  
  From: Implementation steps (Suggested) - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/pages/viewpage.action?pageId=606778658

  Implementation steps (Suggested)

  The procedure described below provides a high-level guideline on applying AppDynamics Application Maturity and its subsequent assessment to achieve a Gold standard. This article is meant to serve as a suggested approach. It's by no means strict. There are plethora of approach and techniques in applying the maturity model and many paths to achieving the Gold standard. 

  Review updated requirements from the application team. Collect as much information from various roles (operations, test management, user experience, previous business impacting incidents, etc.) such as objectives, goals, and pain points.
  Use the information discovered in the previous step to identify enhancement and fine-tuning opportunities against the current AppDynamics instrumentation of the application. 
  Furthermore, the knowledge gleaned about the application from the previous step allows you to make recommendation on advanced AppDynamics feature currently not instrumented to provide more value to the application team in terms of application performance monitoring. 
  Examine each existing instrumentation components for accuracy and completeness that is based on updated requirements.
  Current list of transaction detection rules, service end-points, and error detection.
  Existing list of health rules, policies, and actions.
  Quality of existing telemetry (custom dashboards).
  End-user monitoring applicability.
  Integration of transactions with Analytics.
  Use of APIs to enhance/enrich additional application data sets.
  Perform a final review of updated instrumentation with the application team and complete any remaining identified actions.
  Perform an application assessment to determine maturity categorization (bronze/silver/gold).
  Close gaps by driving required AppDynamics instrumentation to achieve Gold standard.
  Perform follow up assessment, close out gaps, until a Gold standard is achieved. 

# employment.t-mobile # 20240530 # daily # standup

Still working on EDSRE-245
I've been digging through Confluence documentation to determine if there
is a programmatic way to query for wether or not a Business App is 
Argos-Enabled 

Met with Michael Chan to go over BusinessApp-to-APMID mapping in AppDynamics
Next step is to meet with Michael Chan and Michael Reese
to bounce ideas around and see if we can

Requested access to the Enterprise Observability Standard
https://tmobileusa.sharepoint.com/:w:/r/sites/THStabilityInitiatives/_layouts/15/Doc.aspx?sourcedoc=%7B88B88C62-49CC-499D-A3F5-15F1A2740704%7D&file=Enterprise%20Observability%20Standard%20WORKING%20VERSION.docx&action=default&mobileredirect=true

# employment.t-mobile # logmon # perfmon # observability # instrumentation # definition # standards # practices # procedures

  From: Instrumentation - CCO - T-Mobile
  https://confluencesw.t-mobile.com/display/CCO/Instrumentation

    Instrumentation is the act of adding observability code to an app yourself.

    How OpenTelemetry facilitates instrumentation

    In order to make a system observable, it must be instrumented: That is, code from the system’s components must emit traces, metrics, and logs.

    Using OpenTelemetry, you can instrument your code in two primary ways:

    Code-based solutions via official APIs and SDKs for most languages
    Zero-code solutions
    Code-based solutions allow you to get deeper insight and rich telemetry from your application itself. They let you use the OpenTelemetry API to generate telemetry from your application, which acts as an essential complement to the telemetry generated by zero-code solutions.

    Zero-code solutions are great for getting started, or when you can’t modify the application you need to get telemetry out of. They provide rich telemetry from libraries you use and/or the environment your application runs in. Another way to think of it is that they provide information about what’s happening at the edges of your application.

    You can use both solutions simultaneously

# employment.t-mobile # cdp # support # k8s # tagging # policies # cicd # guide # email # communications

  Effective May 1st, 2024,

  Untagged Jobs: All untagged jobs will now run exclusively on K8s runners. Jobs lacking a specific runner tag will be automatically assigned to the k8s-medium runner.
  Deprecated Runners: The docker-machine/aws runners will be deprecated and will require an OPT-IN action to utilize.
  If you encounter any issues with jobs running on K8s, please ensure to tag your jobs to the medium runner.
  Runner Specifications: Please note the following key differences between K8s runners and docker-based runners:
  Sudo/Root/Privileged Access will NOT be available on K8s runners.
  Ensure to refer to the K8s CIDR/Subnet/IPs for egress/ingress IPs. Please raise Firewall Requests to your environments if necessary.
  Job Modifications: It is advised to replace any ping checks with wget on your GitLab jobs.
  References:

  https://cdp.docs.t-mobile.com/getting-started/migrate_to_k8s...
  https://cdp.docs.t-mobile.com/faq/How-Tos/dind_deprecation_k...
  https://cdp.docs.t-mobile.com/faq/Access-Issues/GitLab_Runne...
  https://cdp.docs.t-mobile.com/faq/How-Tos/create_new_firewal...
  https://docs.gitlab.com/ee/ci/yaml/#tags
  Reach out to #cdp-support if your any queries.

# employment.t-mobile # teams # soc

  The SOC drives resolution by reaching out to app teams 
  and working to close the gaps. 

  The APM team provides support related to BT detection, 
  tuning of HRs, etc. 

# employment.t-mobile # pto # ooo # calendar # team # etiquette # pdl # email # addressess # phonebook # public # distribution # list 

  Brian Castle
  I've created an email PDL for this group, it's called "Enterprise SRE Core Team".  Should make it easier to address the group with meeting makers etc.
  
  Team, some housekeeping:  When out of office (PTO etc.), please send a meeting maker to the team PDL that is marked as an All Day Event so we all know not to go hunting for you.  It's super important though to also set it up to show as "Free" or else you'll be blocking everyone else's calendar with your PTO day... and then everyone will grumble at you.  You can always create another one without any recipients just to block your own calendar as Out of Office.

# employment.t-mobile # knowledge # transfer # kt # enterprise # sessions # 20240522 # pacbot # stability # violations # dashboard
  
  Presenter: Kambali Math

  Stability Violations
    By Owner (VP, SVP, Manager Name)
    Type Breakdown
  Rule Compliance Overview
  Single Policy multi-seriality
  Production-data based vulnerabilities
  DPY.07 Asset Indicator on Deployed Apps

  DPY.07 is at the pod level

  APM ID
  If assetBusinessCriticalityLevel == 1


  https://kibana.dev.pac.kube.t-mobile.com/app/dev_tools#/console
  Fields:
    appID: apm0102715
    assetLifecycleStage: Operational
    assetBusinessCriticalityLevel: 1 == critical
    assetCompanyFlag: T-Mobile

  CSDM/CMDP: 
  APM IDs
  Platform-stats API, e.g.
  https://platform-stats.pe.t-mobile.com/latest/api/tke/namespaces
  https://platform-stats.geo.mesh.t-mobile.com/
  ServiceNow

# employment.t-mobile

  Shift-left Northstar
  Steelthread
  dpy05

# employment.t-mobile # incident # response # survey # questions # to # ask
  
  From: Argos Dashboard - Monitoring - ets-tfb-Ordering-Shop-KingsLanding-Team1 - T-Mobile
  https://confluencesw.t-mobile.com/display/ETS/Argos+Dashboard+-+Monitoring

  Is there any impact on subscribers/customers? 
  Is there any impact on T-Mobile personnel/users?
  Actions needed to resolve the issue?
  Estimated time to resolve?
  Actions taken to resolve this issue?
  Possible root cause of this issue? 

# employment.t-mobile # jira # ticket # template # copycat # strategies

  [BSWMSCE2-926] SCE - TEMPLATE STORY - CLONE ALL STORIES FROM THIS ARTIFACT - Jira
  https://jirasw.t-mobile.com/browse/BSWMSCE2-926

# employment.t-mobile # documenation # templates # initiatives # goals # deliverables # features # development # policies # procedures # customer # impact # stakeholders # delivrables

  Overview  Business Value & KPI
  ARGOS Dashboard is a tool that monitors the health and performance of the applications and services.  The dashboard collects and analyzes data from various sources to detect any outages or issues affecting the Frontline or T-Mobile customers. This initiative will enable faster response to outages, reduce the mean time to restore (MTTR), and enhance the customer experience and satisfaction.

  The Customer
    NetPro
    Information Technology
  
  Problem Statement / Opportunity

    The 40% of the alarms from the ARGOS dashboard is non-actionable. Removing the noise and increasing the value of the alerts tied to 5 key flows (Activations, Upgrades, Add A Line, Payments and Service) more actionable.
  
  Goal / Outcome
    Lower the number of P1/P2s, lower the number of non-actionable alarms.
  
  Business Value
  
    Increase uptime for applications used by the Frontline to service T-Mobile customers.

  Tech Value

    By removing the noise with non-actionable alerts, the SOC team can focus on proactive measures for preventing outages to the Frontline and T-Mobile customers.

  Impact if we do not do this Work
  
    The increased risk of outages to the Frontline and T-Mobile customers.

  Project Plan / Hub / Slack

  OKRs/KPIs

    Store Availability >99%
    Store Impact Minutes
    Overtime reduction of 50%

# employment.t-mobile # powershell 

  `@("Bert","Gibran","Samir","Shola","Steve","Tony","Vikas") | Sort-Object {Get-Random}`
  
  Easiest way to Shuffle an Array with PowerShell
  https://ilovepowershell.com/powershell-modern/easiest-way-shuffle-array-powershell/

  curl 'https://techapps.t-mobile.com/esdlvr-argos-layout/controller4/api/widget/v1/widget?id=27351390&hr=2&span=2' \


# employment.t-mobile # terminology # business # processes # bas # business # applications # kf # keyflows # key # flows

  What is a key flow?
  From what I gather, a key flow is an umbrella term encompassing major business processes
  The 5 major key flows are:
  - Activations
    Key Flow - Activation - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Activation 
  - Upgrades (aka Trade-in)
    Key Flow - Upgrade - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Upgrade 
  - Add A Line (AAL)
    Key Flow - AAL - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+AAL
  - Payments
    Key Flow - Payments - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Payments
  - Service

# employment.t-mobile # knowledge # transfer # kt # enterprise # sessions # 20240502 # terminology # itil # data # centers # datacenters

  Impact Radius
  CMDB
  Vulnerabilities
  ITSM
  SRE Maturity
  Observability
  Resiliency

  How do you determine the SRE maturity of an application?
    Monolith vs Microservices
    12 or 15 factor authentication piece
    Alerts
    Logging
    Observability
    Monitoring
    Cloud-native
    Secrets Management (e.g. Rotations)
    Repo security
  Again, two data centers - Polaris & Titan
  These two are asymmetric, as Titan cannot run the same workloads as Polaris
  Should a geoevent occur, and Polaris become unavailable, you would have critical, widespread service outage
  GTM A5s
  LTM A10s

# employment.t-mobile # critical # operations # and # reliability # engineering # core # standards # logging # data # flow # metadata # inclusion # included # required # observability # traceability # process # copycat

  Incident Response tend to be chaotic and requires many SMEs to resolve, when systems don’t emit enough business context signals. It is very important to decorate every transaction with metadata at source systems to understand the business context and impacts. Engineers also should use technologies to stitch the transactions end-2-end, however, without business context, end-2-end tracing may not address many questions. Engineers should be able to answer- From what channel a transaction has originated, which application in that channel started that transaction, to which business flow this transaction belongs, what all systems this transaction touched etc etc...
  These questions can be answered only when our source code is decorated with proper business context information. As part of Quality OKR, P&T is focusing on 5 key business flows- Activations, Add-A-Line, Payments, Upgrades and Trade-In. All the originating systems of these 5 key flows should generate this metadata and propagate to all south bound systems. Receiving systems to consume this information from source system and propagate to their backends.  When systems instrumented with enterprise monitoring tools (AppDynamics), request’s metadata helps in stitching the transactions, understanding the business context. This information and end-2-end stitching significantly reduces the MTTR.
  Here is the guidelines on metadata :
  How do you stitch transaction e2e?
  Header: interaction-id
  Description: Unique Identifier for Customer interaction. It is designed with intent of getting created in first Application, where customer starts interaction. In self-serve channel this may be equivalent to session-id (cartId /orderId could be a substitute in cases where this is known at beginning of business flow).
  Developers either could chose to generate a random GUID or use a cartID/orderID etc…
  This is a mandatory header to be propagated as part of Quality OKR.
  Example: interaction-id: c34e7acd-384b-4c22-8b02-ba3963682508
  How do you know which channel and origin system?
  Header: channel-id
  Description: Unique Identifier of the channel of the request. This ID should be applied at origin system and shouldn’t be altered in any layer. From Quality OKR perspective, channel-id value should be either DIGITAL/RETAIL/CARE.
  Note: Any exceptions to these values need to be tracked as tech debt.
  Example: channel-id: DIGITAL, RETAIL, CARE, IVR etc
  Header: origin-application-id
  Description: Unique Identifier of the source Application of the request. This ID should be applied at origin system and shouldn’t be altered in any layer. Always pass AKMID-<short name>.
  Example: origin-application-id: 1234-DASH
  Header: application-id:
  Description: Unique Identifier of the Application processing the request.  At the source of transaction, both origin-application-id and application-id will be set to same value but when it traversed to next layer, application-id need to be set to it’s own application ID, while making a south bound call. Always pass AKMID-<short name>. Value of this header can be changed at each layer.
  Example: application-id: 2345-TIBCO
  What is the business flow of this transaction?
  Header: workflow-id
  Description: Unique Identifier of the business flow of the request. This ID should be applied at origin system and shouldn’t be altered in any layer. In Quality OKR scope, values for workflow-id would be either of – Activations, Add-A-Line, Upgrades, Trade-in and Payments.
  Example: workflow-id: Activations or Add-A-Line or Upgrades or Trade-In or Payments
  For any other flows, please check FIT tool and select a value from “Journey Scenarios”.
  FIT link: https://app.powerbi.com/groups/me/reports/e8e0765b-6082-40e8-9ab0-82b46594fc80/ReportSection?bookmarkGuid=Bookmark6632d261808415e9e3f5
  What is the activity/sub business flow of this transaction?
  Header: sub-workflow-id
  Description: Unique Identifier of the activity of the request. Underneath every business flow, there may be one or more activities. It is very important to capture that context to understand the impacts in case of any issues. Value of this header can be changed at each layer.
  Example: sub-workflow-id: OTP, Payment Arrangement
  Header: activity-id
  Description: Unique identifier for end to end transaction tracking for a FrontEnd initiated API request. unique per transaction i.e. each API call at the Origin of API chain. Any orchestrated API calls to reuse same ID as received from North bound, for each of such orchestrated South bound calls. NFS GUID is a good candidate for activity-id. Any other business entity like paymentId/postransactionId  etc can be a candidate in few very specific API call that is coupled/direct identifier of that API and each API request has new ID; but it can’t be used in other related API calls ex - authorizeCard, settleCard & postPayment to acct – if all three is needed to achieve payment workflow then we can’t pass paymentId as activity-id
  Developers either could chose to generate a random GUID or use any unique identifier.
  Example: activity-id: c34e7acd-384b-4c22-8b02-ba3963682508
  Header: service-transaction-id
  Description: Unique id for each service/api call between the layer/component. Every component/layer generate unique id for external service call.
  Example: service-transaction-id: acdc34e7-4c22-384b-ba39-c34e7acd508
  Any additional details of this transaction?
  Header: session-id: 
  Description: User session – created upon user (Customer or Rep) login and valid for all calls till Logout. Web login IAM response “usn” value or application session-ids are good fit.
  Example: session-id: 350b91ec-4a64-4b10-a3f3-a78c8db3924a
  Header: baggage-id
  Description: In some cases, we may need additional context or values to understand the transactions the better. For e.g. From which store a transaction has originated or for what customer class, credit check has been performed etc….
  Baggage-id expect name/value pairs as the values.
  Example.: baggage-id: store:1234, os:android, cust_class:I

# employment.t-mobile # knowledge # transfer # kt # enterprise # sessions # 20240425 # northstar # terminology # initiatives
  
  Umbrella term for a set of very large investments in transforming
  T-Mobile B2B infrastructure:
    - Frontend systems
    - Middleware
  Leveraging Salesforce.com to build the UI portion
  Plugging into the backend components with Fuel

  SteelThread
    Plugs in to Fuel
  Unicorn
    Replacing everything around:
      t-mobile.com
        accounts
        buy/upgade phones
      retail system
        presenting offers to customers
        telesales/virtual retail
          Customers can call in and get orders done over the phone
        my-t-mobile phone app
    Frontend for Unicorn
      Vendor: Adobe Commerce Cloud
    Plugs in to Fuel
  Fuel
    Vendor: amdocs
    Backend services that supports the collections/processing of orders
    Customer Repository
    Product Catalog
    Supporting APIs
    Order Handling
  Free Style Billing
    Vendor: amdocs
    Replacing the current billing system named Ensemble (dubbed Samson), from amdocs
    Real-time billing
      Current paradigm:
        Customer makes call
        Call-detail record/call records created
        Batch process picks that up
      New paradigm:
        Instant processing of records
    Pre-paid
      Month to month service
    Post-paid

  Assisted
  Jarvis
    Chat bots
    IVR

# employment.t-mobile # appdynamics # appd # perfmon # online # resources # login

  For the account name, enter in the sub-domain portion as the account number, 
  e.g. `tmo-prod`, then press 'Next'
  This should invoke SSO login

# employment.t-mobile # jira # worklog # EDSRE-245 # pacbot # policy # copycat # inspiration # sre # maturity # index # best # practices # webhook # monitoring # howto # request # devs # keyflows # argos # team # who # nfrs # nonfunctional # requirements # examples # toread # splunk # argos # apis # tke-metadata # repo # tke-cluster-metadata # platform-stats
  
  Parent Ticket:
    [EDSRE-244] Observability Policies - Jira
    https://jirasw.t-mobile.com/browse/EDSRE-244

  [EDSRE-245] Observability Policy: OBS.01: Create Policy for SLO Definition in Argos - Jira
  https://jirasw.t-mobile.com/browse/EDSRE-245

  20240618

  CMG_Public / Blogs / Python Data Collector for ServiceNow Incidents and Changes · GitLab
  https://gitlab.com/cmg_public/blogs/python-data-collector-for-servicenow-incidents-and-changes/-/tree/main?ref_type=heads

  Prakash Parvathaneni / pysnow · GitLab
  https://gitlab.com/prakash.parvathaneni1987/pysnow

  python_servicnow_treatment / snow_incidents · GitLab
  https://gitlab.com/python_servicnow_treatment/snow_incidents/-/tree/main?ref_type=heads

  ansible-summit-connect / ansible-servicenow-demo · GitLab
  https://gitlab.com/ansible-summit-connect/ansible-servicenow-demo

  Zach Villers / servicenow-itsm · GitLab
  https://gitlab.com/villersz/servicenow-itsm

  CMG_Public / Blogs / Using The ServiceNow API To Create Tickets · GitLab
  https://gitlab.com/cmg_public/blogs/using-the-servicenow-api-to-create-tickets

  Steve Cote / snapi · GitLab
  https://gitlab.com/sdcote/snapi

  20240612

  List of Business Applications in SeviceNow

  Business Services | TMUS
  https://tmus.service-now.com/nav_to.do?uri=%2Fcmdb_ci_service_list.do%3Fsysparm_userpref_module%3D56f10ce26fdd6100fd9277f16a3ee453%26sysparm_view%3DApplication%26sysparm_query%3Du_eal%3Dtrue%5EEQ%26sysparm_clear_stack%3Dtrue

  20240611

  Definition of Done:
    For each BA contributing to a KF, 
    define a compliance discovery for ensuring the BA 
    is associated with the health indicators for those KFs in Argos. 
    APM service health indicators for the individual BAs should be 
    included in the overall health indicator for the KFs that they implement.

    Provide a reference to Enterprise standards or create the 
    standard if it doesn't exist.

  Notes:

  business use cases of your application. For example, Checkout, Find Account, Create Problem, etc. 
  This will be the starting point to instrument, detect critical transactions, and define alarming for your appication in AppDynamics.

  20240530

  This is what apps need to be doing that
  What apps need to be setup in Argos
  Correlate to an APMID
  SLO defined at application level

  20240530

  Notes from Vikas

  Dave Cornet
    Correlate 
    SLO defined in Argos
    Link Policy to an APM
  If you have an APM
  Should be enabled 
  Mast applications 
    UI sits on top of Argos configuration
      

  Similar effort by Michael Chan?
  AppMaturity > T-Mobile - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/display/ESAKB/T-Mobile

  Meeting with Michael Chan

  APM Record 
    APMID
  APPD
    Description Field: APMID Mapping

  To Read:

  Best Practices and Recommendations - ESA Knowledge Base - T-Mobile
  https://confluencesw.t-mobile.com/display/ESAKB/Best+Practices+and+Recommendations

  Pipelines, TKE Metadata and Platform-stats - Joe Lupo - T-Mobile
  https://confluencesw.t-mobile.com/display/~JLupo@gsm1900.org/Pipelines%2C+TKE+Metadata+and+Platform-stats

  (ESD)ETL - EIS Performance - T-Mobile
  https://confluencesw.t-mobile.com/display/EISP/%28ESD%29ETL 

  Accelerators - CCO - T-Mobile (Powerpoint Presentation on Observability Goals, along with Webex recording)
  https://confluencesw.t-mobile.com/display/CCO/Accelerators

  20240524

  - References:
    - title: Argos Microservices - ESA Knowledge Base - T-Mobile
      url: https://confluencesw.t-mobile.com/display/ESAKB/Argos+Microservices
      notes:
        - Microservices (with links to their gitlab repos):
          argos-layout-app: Angular app/UI for Argos
          https://gitlab.com/tmobile/TSM/argos-layout-app
          Docker generates artifacts that are hosted in ngix
          argos-widget-api
          https://gitlab.com/tmobile/TSM/argos-widget-api
          argos-drilldown-api
          https://gitlab.com/tmobile/TSM/argos-drilldown-api
          argos-config-api
          https://gitlab.com/tmobile/TSM/argos-config-api
    - title: Argos Aggregated Splunk Query Onboarding - ESA Knowledge Base - T-Mobile
      url: https://confluencesw.t-mobile.com/display/ESAKB/Argos+Aggregated+Splunk+Query+Onboarding
      notes:
        - Step-by-step guide on the onboarding process, splunk, etc
    - title: ARGOS Dashboard Evolution - Service Operations Center - T-Mobile
      url: https://confluencesw.t-mobile.com/display/ServiceOperationsCenter/ARGOS+Dashboard+Evolution
      notes:
        - deliverables
    - title: 250.49 - AIMS+ Integration - Network Supply Chain - T-Mobile
      url: https://confluencesw.t-mobile.com/pages/viewpage.action?pageId=123982700
      notes:
        - List of NFRs
    - title: Enterprise Solutions Delivery - ESA Knowledge Base - T-Mobile
      url: https://confluencesw.t-mobile.com/display/ESAKB
      notes:
      - RE: Argos team - For general questions, please use our Teams channels. (do not ping induvial engineers for help - they will guide you to Teams or open a JIRA intake)
      - AppDynamics (Customer) 
      - Argos (Customer
      - Open Text (customer)
      - Revealx (Customer) 
    - title: Onboarding to Argos Process - Metro by T-Mobile - T-Mobile
      url: https://confluencesw.t-mobile.com/display/MBT/Onboarding+to+Argos+Process
      notes:
        - What is Argos?
        - Argos Team
        - Nikolova, Gabriela 
        - Boeck, Andor 
        - Earley, Anderson 
    - title: Key Flows and Scope - Sivaraman Varatharajan - T-Mobile
      url: https://confluencesw.t-mobile.com/display/~SVarath1@gsm1900.org/Key+Flows+and+Scope
      notes:
        - Keyflows
    - title: (ESD)Webhook - EIS Performance - T-Mobile
      url: https://confluencesw.t-mobile.com/display/EISP/%28ESD%29Webhook
      notes:
        - How to request monitoring of your application via Webhook?
    - title: Enterprise Standards - IT-DDA - T-Mobile
      url:   https://confluencesw.t-mobile.com/display/ITDDA/Enterprise+Standards
    - title: Supply Chain Service Backlog - Supply Chain Execution - T-Mobile
      url: https://confluencesw.t-mobile.com/display/SCE/Supply+Chain+Service+Backlog
      notes:
        - Security: Provide secure API access and execution in order to protect sensitive production data.
        - Business Journeys: Data flow and business Journey
        - Technical Architecture
        - Vulnerability 
        - Resiliency
        - MFA: Use strong authentication and authorization for user accounts in order to reduce the risk of unauthorized access.
        - Log Management
        - Testing Automation
        - Monitoring and Alerting: Telemetry
        - SOX Compliance
        - Business Process Monitoring and Reconciliation: Balance and reconciliation
        - Product Launch's and NPI 
        - API Security
        - Ops Sustainment
        - Regression and Manual testing
        - Change Management, NOD, HOTS and Hypercare
        - Release Management,
        - Incident Management
        - Problem Management


  20240522
  
  Sent meeting invite to Steve:

  I'd like guidance on how to complete the JIRA ticket in the subject line:

  [EDSRE-245] Observability Policy: OBS.01: Create Policy for SLO Definition in Argos - Jira
  https://jirasw.t-mobile.com/browse/EDSRE-245

  Questions I'll need to be able to answer in order to complete the work:

  What is a key flow?
  From what I gather, a key flow is an umbrella term encompassing major business processes
  The 5 major key flows are:
  - Activations
    Key Flow - Activation - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Activation 
  - Upgrades (aka Trade-in)
    Key Flow - Upgrade - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Upgrade 
  - Add A Line (AAL)
    Key Flow - AAL - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+AAL
  - Payments
    Key Flow - Payments - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Payments
  - Service

  How do I know when a business app is contributing to a key flow?

  https://tmobileusa.sharepoint.com/:x:/t/BusinessArchitecture/EekdnemVL2tCqXzxaHb14WAB1M0OPLvBJr2sXdA6sVDdDg?e=Tsjklh&CID=EA578052-1E2D-4A8F-83A0-BB356AFD346E&wdLOR=cC9BAAD46-4A81-4F3C-BB54-A9756FCDD0D0

  How do I construct a compliance discovery that ensures the business app 
  is associated with the health indicators for the key flow(s) in Argos?

  What is an APM service health indicator?  Can I see an example of one?

  Where can I find existing Enterprise standards for a given business app?

  Is the below link relevant?
  Enterprise Architecture Principles
  https://devcenter.t-mobile.com/documents/60f7143398b00500088587a0?name=Enterprise-Architecture-Principles&filePath=/Principle-Documentations&sectionName=Principle-Documentations   

  If no Enterprise standard exists, where can I find an example of one?

  Is the below link relevant?
  Enterprise Architecture Principles
  https://devcenter.t-mobile.com/documents/60f7143398b00500088587a0?name=Enterprise-Architecture-Principles&filePath=/Principle-Documentations&sectionName=Principle-Documentations   

  How do I know what belongs in Argos or not?

  What are indicators of a business app being registered in Argos?

  20240521

  Questions I'll need to be able to answer in order to complete the work:

    What is a key flow?
      A key flow is an umbrella term encompassing major business processes
        - AAL
          Key Flow - AAL - B2B - T-Mobile
          https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+AAL
        - Activation
          Key Flow - Activation - B2B - T-Mobile
          https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Activation       
        - Payments
          Key Flow - Payments - B2B - T-Mobile
          https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Payments       
        - Upgrade (aka Trade-in)
          Key Flow - Upgrade - B2B - T-Mobile
          https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Upgrade        
    How do I know when a business app is contributing to a key flow?

    How do I construct a compliance discovery that ensures the business app is associated with the health indicators for the key flow(s) in Argos?
    What is an APM service health indicator? 
    Where can I find existing Enterprise standards for a given business app?
      Enterprise Architecture Principles
      https://devcenter.t-mobile.com/documents/60f7143398b00500088587a0?name=Enterprise-Architecture-Principles&filePath=/Principle-Documentations&sectionName=Principle-Documentations   
    If no Enterprise standard exists, where can I find an example of one?
      As above?
    How do I know what belongs in Argos or not?
    What are indicators of a business app being registered in Argos?

  20240517

  The document I'll be updating is:
  https://gitlab.com/tmobile/sre/polstd/sre_docs/-/blob/tmo/main/content/policies/observability.md  

  Acceptance Criteria:

    PacBot policy to evaluate proper Argos inclusion of all 
    Business Apps implementing Key Flows
    "Proper Inclusion" for this story is that each BA has at least 1 
    active metric registered for each KF that it participates in.
    The set of BAs in scope for this discovery is automatically maintained 
    (won't go stale and is included in BA lifecycle mechanisms). 

  Abbreviations

  BA - Business Application (ServiceNow construct)
  KF - Key Flow  

  For each BA contributing to a KF, define a compliance discovery for ensuring the BA is associated with the health indicators for those KFs in Argos. APM service health indicators for the individual BAs should be included in the overall health indicator for the KFs that they implement.

  Provide a reference to Enterprise standards or create the standard if it doesn't exist.

  Prerequisites

  Definition of "what belongs in Argos": see  Key Flow to Application Mapping or the list of BAs in the "KF/FL" CR Risk Category in the App Inventory & CR Risk Category Report
  Definition of what "registered in Argos" looks like that can be determined programmatically
  Mechanism for maintaining this list over time
  Automatic Argos inclusion/exclusion as BAs are added/removed from KFs, or as new BAs associated with are created. 

# employment.t-mobile # rca # analysis # meeting # prt 

  Performance Testing
  Defects (E2E and PRT) - Jira
  https://jirasw.t-mobile.com/secure/Dashboard.jspa?selectPageId=99700  

  Areas of improvement

# employment.t-mobile # tony # phong # le # worklog # edsre

  - **Description**
      - **Why?** Why do we need or have this policy? What are the benefits?
     To minimize Downtime and Risk. Safe updates for Infrastucture and Configuration. 
      - By deploying a new version of your application to a separate cluster (the "green" one), 
        you can thoroughly test it before switching traffic over from the production cluster (the "blue" one). 
      This allows you to identify and fix any issues without impacting your live users.  
      In case of problems, you can simply roll back traffic to the previous version running on the blue cluster.
      - Blue-green deployments aren't just for application updates. 
      They can be used to make changes to the underlying AKS cluster infrastructure or configuration itself. 
      This could involve updating Kubernetes versions, modifying network policies, or introducing a new service mesh. 
      By deploying these changes to a green cluster first, you can validate their functionality without affecting your production workloads.  
      This keeps your production environment stable and minimizes the risk of introducing unexpected issues.
      - **How?** How are we going to get compliance on this policy? Which process or tool can be leverage for compliance?
    Deploy to a unused blue state which can be tested and verified without impacting green/live state. switch traffic to green if all verifications are successful
    We can use pipeline job naming conventions as verification or rely on kubernetes/pipeline log information that validates the execution
      1. Set up the newly updated, green environment with the latest app version.
      2. Move the traffic to the green environment once testing and confirmation have been completed.
      3. If there are any problems in the new, green main environment, traffic can then be redirected to the blue environment.
      4. Stop the blue deployment if there is no problem.
      - **What?** What standard is being utilized to create this policy against? Provide excerpt of the standard and a link to review details.
    Utilize CDP recommended deployment job in deployment pipeline. [Click here](
  https://cdp.docs.t-mobile.com/documentation/PCF/#bluegreen-deployment-strategies)
  for standard.
      - **Who?** Follow RACI to prescribe stakeholders and their roles
    DevOps responsible for developing and manage CI/CD pipelines. Manage config changes using Helm
    Infra responsible for provisioning AKS/EKS
    App Dev responsible for app code
    Security responsible for security best practices provisioning AKS/EKS and conduct security assessments of new app version
    Operations responsible for overseeing entire deployment process and ensure smooth transition between blue/green clusters
  - **Metrics** Specify systems that have data points to allow measuring the complaince of this policy. 
    Provide enough information to guide automated compliance engine like PacBot to collect right data for this policy. 
    [B/G deployment governance dashboard](
  https://grafana.cdp.t-mobile.com/d/J_9gdaeWz/gitlab-deployments?orgId=1)
  To gather more details surrounding the inquiry above.

# sre # phonebook # who # teams

  Square: Q/A, Testing?

# sre # onboarding # kt # knowledge # transfer # slack # channels

  frontline_self-service-exec_comms

# employment.t-mobile # powershell # profile.ps1

  ```powershell
  $env:PATH = "$($env:USERPROFILE)\OneDrive - T-Mobile USA\bin;$($env:PATH);$($env:LOCALAPPDATA)\chocolatey;$($env:LOCALAPPDATA)\chocolatey\bin;C:\Programdata\chocolatey\bin;$($env:USERPROFILE)\OneDrive - T-Mobile USA\bin;C:\ProgramData\chocolatey\lib\kubernetes-cli\tools\kubernetes\client\bin;C:\ProgramData\chocolatey\lib\eksctl\tools;C:\ProgramData\chocolatey\lib\k9s\tools;C:\Program Files\Amazon\SessionManagerPlugin\bin"
  $env:REQUESTS_CA_BUNDLE = "$HOME\OneDrive - T-Mobile USA\Documents\workspace\pem\cert.pem"
  $env:AWS_CA_BUNDLE = "$HOME\OneDrive - T-Mobile USA\Documents\workspace\pem\cert.pem"

  Set-ExecutionPolicy Unrestricted -Scope CurrentUser
  Import-Module "$($env:USERPROFILE)\OneDrive - T-Mobile USA\Documents\WindowsPowerShell\Modules\PSReadLine"
  Set-PSReadLineOption -EditMode vi
  Set-PSReadlineKeyHandler -Key Ctrl+r -Function ReverseSearchHistory -ViMode Insert
  Set-PSReadlineKeyHandler -Key Ctrl+r -Function ReverseSearchHistory -ViMode Command
  Set-PSReadLineOption -HistorySearchCursorMovesToEnd
  Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
  Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

  . "$($env:USERPROFILE)\OneDrive - T-Mobile USA\Documents\WindowsPowerShell\Scripts\ConvertTo-Markdown.ps1"

  function subl {
    param(
    [Parameter(
        Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true
        )]
    [Alias('FullName')]$FileName
    )
    &"$($env:USERPROFILE)\OneDrive - T-Mobile USA\tools\Sublimetext4\subl.exe" -a $FileName
  }
  ```

# sre # day # 4 # meeting # 

  standards = interface, policies = instances/implementation

  confluence
  devcenter
  servicenow
  git pages

# sre # onboarding # day 4 # 4 # errors # breakfix # troubleshooting

  Please follow the below steps and try to reinstall when the steps are complete.

  Restart your system
  Rebooting generally will remove any stuck processes.
  Make sure you're connected to the T-Mobile VPN if off site. 
  e.g. central.vpn.t-mobile.
  Load the Command Prompt on your system. 
  Once loaded, enter the command "gpupdate /force" 
  This will force a Group Policy update on the system.
    Below is the initial entry
      Completed process
  Next, load the Control Panel
  We want to get to the Configuration Manager. 
  To do so, select the 'View by' list to change it to Large Icons 
  (some may have this as the default). 
  From there select Configuration Manager.
  Once loaded, select the 'Actions' tab and run the following actions:
    Application Deployment Evaluation Cycle
    User Policy Retrieval & Evaluation Cycle
    Windows Installer Source List Update Cycle
  Once those are running, give it about 15 minutes for the system to complete this function in the background. 
  Once done, re-launch the Software Center and you should be clear to 
  re-install the software you need.

# sre # rca # defects # meeting # jira # dashboard

Defects (E2E and PRT)
https://jirasw.t-mobile.com/secure/Dashboard.jspa?selectPageId=99700

Project: Release Managament
Status:
  - Closed
  - Done
Labels:
  - PI10_PRT_04.25

RM-5755 - ProdPI10_SteelThread_Sales_TOGA Error after Credit Request Submission
RM-5700 - Prod_PI9_SteelThread_Sales_NON-BYOD_PSIM_EIP_Incorrect prices showing up on Review Order Page for Rep and the Customer

TEST ISSUES: TEST DATA PREPARATION
1. What system issue was observed by developer in the defect?
Manual error in communication on what TaxID needs to be mapped between 3 teams. 
This was reference data which we are doing work around to mimic prod behavior (given it's setup for a test account) 
and when 3 teams were talking on a chat Samon - TDM- MDM it was translated incorrectly.
2. What was done to resolve the defect? 
Tax ID was updated in MDM 
3. Are there any underlying issue(s) that might have (or will) cause similar issues? No
4. What should occur (in future) to eliminate or reduce similar defects? Is this new or recurring?
Suggestion from SRE team (etejeda): Eliminate inconsistencies/manual steps
5. If code fix was applied, what is the link to the Merge Request used? NO CICD

# sre # phonebook # emails

Sqar: e2e testing
Release Management: release management

Any defects you see under rm project (post-prod)
Post-release testing defects
Any defects you see under rs project (pre-prod)
System integration
E2E bugs

post release testing, e.g.
prt_04.25

Salesforce
- copado pipelines (CICD)
- User stories, e.g. US-NNNNN
Amdocs
- gitlab pipelines
- gitlab pipeline link

# sre # phonebook # emails

email PDL for this group, it's called "Enterprise SRE Core Team".

# sre # rca # meetings # defects # day 3 # 3 # release # engineering

PRT

Link to Amdocs, e.g. RM-5762
Link to Salesforce tickets, e.g. RM-5766

Where can I find User Stories, e.g. US-0011299
RE: Release Practice
- What are code packages

Release Engineering
Andrew Petro
Lisa Morrow
Luz Aiello
Shravya Nellutla

End to End (E2E) Team

RCA Questionnaire
Following questions can be used as a guidance to streamline Root Cause Analysis.

What system issue was observed by developer in the defect?
What was done to resolve the defect?
Are there any underlying issue(s) that might have (or will) cause similar issues?
What should occur (in future) to eliminate or reduce similar defects? Is this new or recurring?
If code fix was applied, what is the link to the Merge Request used? "NO CICD" if not using CICD for code fixes.


# sre # onboarding # day 3 # 3 # about # company # team # organization

Organization:
CIO: Cano Nester
SVP (OPS): Aravind Manchireddy came from FIS
VP (OPS): Tamara Hall
Sr. Director (Inc/Chg Mgmt): Wendy Wheat
Director (Architecture): Matt Barrett
Sr. Mgr (Architecture): Brian Castle
- 6 contractors/5 FTEs soon to be hired = 11 total workers under Brian Castle
- Half PST/Half CST
- Major ops Seattle
- Frisco/Overland Park, Atlanta locations
- 22 years at Tmobile
- Former developer
- High level solutions design
- Azure portfolio
- SDLC
MTS: Steve Kolski
MTS: Vikas Verma
Steve/Vikas/Brian core of the new SRE team
Front Line Systems: 

$NameList="Bert","Gibran","Samir","Shola","Steve","Tony","Vikas"

Get-Random -InputObject $NameList

# sre # onboarding # day 2 # 2 # access # requests # support # zpa # zscaler # support # tickets # errors # breakfix # troubleshooting

Submitted a ZScaler Support Request, as per https://compass.t-mobile.com/compass?id=kb_article&table=kb_knowledge&sysparm_article=KB00106267#h_263686603811699550459784

What do you need assistance with? I'm having trouble accessing a website
What appears on the screen when the website fails to load? Blank Screen
Is Zscaler Private Access enabled? I don't have ZScaler private access
Where were you located when you faced the issue? Remote
Do you have any additional internal network connection enabled? No other connection enabled
What is the error message displayed?
I'm not able to access VPN-protected t-mobile resources.  For example, when I try to access https://devcenter.t-mobile.com, I encounter the following error message:

This page isn’t working devcenter.t-mobile.com didn’t send any data.
ERR_EMPTY_RESPONSE

From the error messages I'm seeing, it looks to me like my ZScaler VPN connection is not functioning properly, so I'm flagged as though I'm coming in as an external device from the internet at large. Additionally, there is no Private Access section that I can see from the ZScaler app.
What is the full URL you are trying to reach? https://devcenter.t-mobile.com

# sre # policies

From Madhuri: 

Hi Everyone based on discussion with Vikas and steve last week We identified the team that each policy would have to collaborate with. please make a note and i'll have the connects setup when needed


DPY01/02 - CDP/Chris POC
DPY03- CDP /TKE
DPY04 - TKE/Conductor
DPY06- CDP

see: 
https://teams.microsoft.com/l/message/19:meeting_NDMwOTczYTktNjM3My00ODQ5LTllYjItOTM0NDk3ZWFiOGEz@thread.v2/1715093123782?context=%7B%22contextType%22%3A%22chat%22%7D

# employment.t-mobile # jira # url

https://jirasw.t-mobile.com/secure/WelcomeToJIRA.jspa


# employment.t-mobile # documentation # incident # lifecycle

https://confluencesw.t-mobile.com/display/MSMS/Incident+Lifecycle

# employment.t-mobile # account # access # requests

PACT JiraSD : Internal : Access : User
https://myaccess.microsoft.com/@TMobileUSA.onmicrosoft.com#/access-packages/4cf57c4f-4d47-46d0-9fe1-937d464394f4
Business Justification:
I am a member of Brian Castle's SRE team, and require access to this instance of JIRA for my job duties.

PACT JiraSW (SCIM) : Internal : Access : User 
https://myaccess.microsoft.com/@TMobileUSA.onmicrosoft.com#/access-packages/97d26cd2-0a8c-405f-8a07-76052a86b886

As per Support through JiraSD
https://cdp.docs.t-mobile.com/Support/Support_with_JiraSD/
I am a member of Brian Castle's SRE team, and require access to this instance of JIRA for my job duties.
Code Repository: https://gitlab.com/tmobile/sre/polstd
L2/L3 AssetID: APM0501292

PACT Jira Align : Internal : Access : Users
https://myaccess.microsoft.com/@TMobileUSA.onmicrosoft.com#/access-packages/1885a7d2-01e7-4448-acc8-40dd61f69df4

# sre # onboarding # incident # track # servicenow # unable # install # software # center

INC77129503

# sre # onboarding # meeting # with # brian

bellvue wa
Arvind manchireddy
wendy byram
vikas verma - MTS
steve kolski - MTS
matt barrett
brian
Madhuri Reddy (PM)
Samir Shamsi
Shola Babalola
Gibran Igbal
Tony Le
Bryan Owens
Bert Tejeda

NTW Non-T-Mobile Worker
NTID - 
We support products that power sales
Billing
Order entry
Retail systems
Care centers
Support Calls
Underlying system
Customer records
Promotion engines

# sre # accounts # access # gitlab

  https://teams.microsoft.com/l/message/19:meeting_YWRmOWJjZTgtODUyNy00MzNjLTgxODQtMTQzMTNmOGNjOWIw@thread.v2/1714662160642?context=%7B%22contextType%22%3A%22chat%22%7D

# sre # accounts # access # gitlab

RE:  https://gitlab.com/tmobile/sre/polstd
After getting access to Gitlab, Vikas have to add you to the project.

# sre # terminology # terms # acronyms # pacbot # what # is # meaning # definitions # define # business # apps # bas # application # decomposition

APM: Application Performance Monitoring
AKMID:
APMID: Amdocs Partner Management ID
APP: Application running in a specific environment
APPID: Application Service ID
APPLID: Application Service ID
Argos: Argos is a single pane of glass dashboard solution provided by ESD to create one place for leadership, the SOC and MIM to visualize the health of key IT services at T-Mobile. It integrates service health KPIs from BSM, Splunk, AppDynamics, and a variety of other local application monitoring solutions to compile them into service health scores that are visualized, turned into alarms, and stored for executive reporting. The SOC and MIM drive and escalate all Argos alarms to resolution, as well as manage executive communications of open incidents reflected on Argos.
Atlas: Used by customer service agents
BPM: Business Process Monitoring
BA: Business Application -- A logical representation of all software and/or date store instances (Deployable Instance) and infrastructure used to provide one or more Business Capabilities
- references: 
  - title: Application Decomposition - Pristine Playbook - T-Mobile
    url: https://confluencesw.t-mobile.com/display/PPO/Application+Decomposition
BT: Business Transactions
CCO: Cisco Cloud Observability
  - uses:
     - Tracing
Conductor: Kubernetes Cluster, likely EKS, Runs in AWS
CDP: Continuous Deployment Platform
CSDM: Common Service Data Model
Deployable Unit: a non-versioned logical grouping of business functionality and/or data that is packaged/built together and represents a separately deployable part of an application. An application can have 1 or more deployable units.
- references
  - title: Application Decomposition - Pristine Playbook - T-Mobile
      url: https://confluencesw.t-mobile.com/display/PPO/Application+Decomposition        
    notes: |-
        3 types of Deployable Units:
        - Application Component: An Application Component implements the business logic of an Application and represents the lowest level track able and separately deployable part of the Application.
        - Microservice: A Microservice is the deployable component of an architectural style that structures an application as a collection of services (i.e. Microservice) that are: Highly maintainable and testable, Loosely coupled, Independently deployable, organized around business capabilities, and owned by a small team.
        - API (Application Programming Interface): An API is a software intermediary that allows two applications to talk to each other. Essentially, an API is the messenger that delivers a request to the provider and then delivers the response back. There are different types of APIs such as REST and SOAP.                  
Dash: Digitally Assisted Sales Hub
      Used to mock certain key flows
Dexter: Tool created by APPD Engineers to extract Configuration , Metrics , and Business Transaction Data and organize into a Data Warehouse or Report form
  see:
    DEXTER - APPDynamics Data Extract and Reporting Tool (SETUP & USAGE) - ESD DevOps Team Home - T-Mobile
    https://confluencesw.t-mobile.com/pages/viewpage.action?pageId=586204350  
Key Flow: A Key Process Flow, or Key Flow, is an umbrella term encompassing any major business process
  The 5 business critical key flows are:
  - Activations
    Key Flow - Activation - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Activation 
  - Upgrades (aka Trade-in)
    Key Flow - Upgrade - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Upgrade 
  - Add A Line (AAL)
    Key Flow - AAL - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+AAL
  - Payments
    Key Flow - Payments - B2B - T-Mobile
    https://confluencesw.t-mobile.com/display/B2B/Key+Flow+-+Payments
  - Service 
MELT: Metrics, Events, Logs, Traces
MPOS: Mobile Point of Sale
MTS: Member of Technical Staff is what the title stands for, it's a level above Principal
NFR: Non-Functional Requirements
NNF: Near-Near-Far
     Deployment strategy for local applications
     Pertains to two data centers (Polaris(primary), Titan)
     Local Redundancy at the data center level as well as disaster recovery
NOVA: A Self-Service Asset Management Tool
      Its where you create/modify/delete an asset
Observability: Simply stated, it is the ability to measure and understand what is happening within and across environments and processes so that T-Mobile can quickly detect and resolve issues to keep our systems efficient, reliable and our customers happy.
OOB: Out of the Box,  Functionality that is part of the basic Amdocs product(s)
PacBot: Policy as Code Bot. 
        It is a platform for continuous compliance monitoring, 
        reporting and security automation for the cloud. 
        Security and compliance policies are implemented as code 
        and all cloud assets are evaluated against these policies 
        to gauge compliance.
PRT: Performance Readiness Testing
RED: Rate, Error, Duration (Metrics)
- references: 
  - title: The RED Method: A New Approach to Monitoring Microservices - The New Stack
    url: https://thenewstack.io/monitoring-microservices-red-method/  
  - title: Atlas - RED Metrics - CED_Platform - T-Mobile
    url: https://confluencesw.t-mobile.com/display/Notifications/Atlas+-+RED+Metrics
SLO: Service Level Objective
TKE: T-Mobile Kubernetes Engine: 
  - Runs on-prem
  - Vanilla K8s, self-managed 
USE: Utilization, Saturation, and Errors (Metrics)
VOCE: Vendor Owned Company Enabled Device - this is how you've been set up, the alternative being Magenta Desktop (Citrix)

  see:
    Acronyms & Definitions - Billing & Technical Product Solutions - T-Mobile
    https://confluencesw.t-mobile.com/pages/viewpage.action?pageId=229823686
    PrES: Pacbot Violations Management - DataProtectionAndTechnologyCompliance - T-Mobile
    https://confluencesw.t-mobile.com/display/DPCT/PrES%3A+Pacbot+Violations+Management

# sre # terminology # products # acronyms 

https://teams.microsoft.com/l/message/19:bd257ad360674551888f8b5dbf5e8497@thread.v2/1714400383484?context=%7B%22contextType%22%3A%22chat%22%7D

# sre # terminology # products # acronyms

DASH: Digitally Assisted Sales Hub, the primary Postpaid frontline order capture system for Retail

Rebellion: One of the primary frontline order capture systems for Prepaid, also in Retail.  Mostly serves our "Magenta Prepaid" brand, which is not to be confused with our Metro by T-Mobile prepaid brand.

Magenta: Citrix Desktop, used by contractors